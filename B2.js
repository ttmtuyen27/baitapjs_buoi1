/**
 * Input: 5 số thực
 *
 * Step:
 * -B1: tạo 5 biến chứa 5 giá trị input
 * -B2: tạo biến chứa kết quả
 * -B3: tính giá trị trung bình theo công thức: giaTriTrungBinh=(num1+num2+num3+num4+num5)/5
 * -B4: In kết quả trung bình ra màn hình
 *
 * Output: giá trị trung bình
 *
 */

var num1 = 1,
  num2 = 2,
  num3 = 3,
  num4 = 4,
  num5 = 5;

var result = null;
result = (num1 + num2 + num3 + num4 + num5) / 5;
console.log("trung binh cong: ", result);
