/**
 * Input: Chiều dài và chiều rộng
 *
 * Step:
 * -B1: Tạo 2 biến chứa chiều dài và chiều rộng
 * -B2: Tạo 2 biến chứa kết quả diện tích và chu vi
 * -B3: Tính chu vi và diện tích theo công thức
 *      + chuVi = (dài + rộng)*2
 *      + dienTich = dài*rộng
 * -B4: In kết quả ra màn hình
 *
 *
 * Output: Chu vi và diện tích
 */

var chieuDai = 2,
  chieuRong = 4;
var chuVi = null,
  dienTich = null;
chuVi = (chieuDai + chieuRong) * 2;
dienTich = chieuDai * chieuRong;
console.log("chu vi: ", chuVi, "dien tich: ", dienTich);
