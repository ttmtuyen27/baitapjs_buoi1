/**
 * Input: Nhập số tiền theo USD
 *
 * Step:
 * -B1: Tạo 1 biến chứa giá trị USD
 * -B2: Tạo 1 biến chứa giá trị chuyển đổi ra VND
 * -B3: Tính giá trị chuyển đổi theo công thức: VND=USD*23500
 * -B4:In kết quả ra màn hình
 *
 * Output: Số tiền chuyển đổi ra VND
 */

var USD = 2;
var VND = null;
VND = USD * 23500;
console.log("VND: ", VND);
