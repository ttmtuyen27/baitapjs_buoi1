/**
 * Input: 1 số có 2 chữ số
 *
 * Step:
 * -B1: Tạo 1 biến chứa số nhập vào
 * -B2: Tạo 1 biến chứa tổng 2 chữ số
 * -B3: Lấy (input % 10) + (Giá trị làm tròn của (input / 10)) -> gán vào biến kết quả
 * -B4: In kết quả ra màn hình
 *
 * Output: Tổng 2 chữ số
 */

var input = 24;
var output = null;
output = (input % 10) + Math.floor(input / 10);
console.log("tong 2 chu so: ", output);
